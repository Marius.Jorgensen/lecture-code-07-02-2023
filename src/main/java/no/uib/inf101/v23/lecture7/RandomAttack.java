package no.uib.inf101.v23.lecture7;

import java.util.Random;

public class RandomAttack implements IMove{

	Random random = new Random();
	
	@Override
	public int computeDamage(IPokemon attacker, IPokemon defender) {
		return attacker.getStrength() + random.nextInt(defender.getStrength()/2);
	}

}
