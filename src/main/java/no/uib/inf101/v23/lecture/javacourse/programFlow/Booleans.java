package no.uib.inf101.v23.lecture.javacourse.programFlow;

public class Booleans {
    
    public static void main(String[] args) {
        boolean a = true;
        boolean b = true;
        boolean c = true;

        boolean result = a || ((b && !c) && (!b || !c));
        System.out.println(result);

        if (a == b) {
            System.out.println("Kake");
        }
        else if (a == c) {
            System.out.println("vaffel");
        }
        else if (true) {
            System.out.println("Pai");
        }
    }

}
